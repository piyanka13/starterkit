-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2016 at 09:10 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_client` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_client_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '2',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `logged_at` int(11) DEFAULT NULL,
  `nip` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `access_token`, `password_hash`, `oauth_client`, `oauth_client_user_id`, `email`, `status`, `created_at`, `updated_at`, `logged_at`, `nip`) VALUES
(1, 'webmaster', '3_jMvONNce5PpC2XXUdhJm3oFJ1gjdxR', '-sUCLLWkEmUUuMH-4b5Zp60jUzAsBR2ETlyuZgWI', '$2y$13$xHz3BPI.hqogi1yhSq6oHOKmmVy1uuvi/vn19brsTs9H7JGFZp3Aa', NULL, NULL, 'webmaster@example.com', 2, 1475243967, 1475593410, 1475607319, '023213'),
(2, 'manager', 'VvFZKi7NKH_pEaE4bGO8Na61c9M8ErKg', 'E_Jk0ShsvKLX8YX5t9m3HvxK_QWOmx8WQMIC3rPa', '$2y$13$13xgKYdr7XZ.DBynfs6hqe4DBsYK2seSSflFj67SGQJ.kDi2GoeD2', NULL, NULL, 'manager@example.com', 2, 1475243968, 1475593403, 1475607451, '0122'),
(3, 'user', 'TyyKemmMTb5ooS_ndFrP_WoV-3TParBs', 'yoxdOS6MM2A7_3iZUpaB_q66kQiDXqfDEh_xmbcj', '$2y$13$5caHlqYooIPo7xCu/MBKoOCpGkVVN0xOsHqq7EI2xKshNuyadZAG6', NULL, NULL, 'user@example.com', 2, 1475243968, 1475593393, 1475561607, '111111'),
(4, 'priyanka', 'nHtTjbvifEn4Uy5o9valtAfC12s1OW2r', 'mqk06M4caqkPbLhNlkKzQ6NTxnMDX5qZRHDTKQNS', '$2y$13$on5//bDnMxuJsAWdzvEIQO2E9hR/JbKyS2EWAVWgw0kr8Zk.pTJqW', NULL, NULL, 'priyanka_sanjay@yahoo.com', 2, 1475557978, 1475593376, 1475557979, '12389');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
