-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2016 at 03:10 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmanager2`
--

-- --------------------------------------------------------

--
-- Table structure for table `waktu_kerja`
--

CREATE TABLE `waktu_kerja` (
  `id` int(4) NOT NULL,
  `nip` varchar(255) NOT NULL,
  `jam_awal` datetime NOT NULL,
  `jam_pulang` datetime NOT NULL,
  `deskripsi` text,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `waktu_kerja`
--

INSERT INTO `waktu_kerja` (`id`, `nip`, `jam_awal`, `jam_pulang`, `deskripsi`, `is_deleted`) VALUES
(1, '', '2016-10-04 00:00:01', '2016-10-04 00:00:01', '1', 1),
(2, '', '2016-10-04 10:15:00', '2016-10-04 10:15:00', '1', 0),
(3, '2', '2016-10-04 09:45:48', '2016-09-27 05:45:48', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `waktu_kerja`
--
ALTER TABLE `waktu_kerja`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `waktu_kerja`
--
ALTER TABLE `waktu_kerja`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
