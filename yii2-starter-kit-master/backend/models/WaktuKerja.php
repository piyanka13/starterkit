<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "waktu_kerja".
 *
 * @property integer $id
 * @property string $nip
 * @property string $jam_awal
 * @property string $jam_pulang
 * @property string $deskripsi
 * @property integer $is_deleted
 */
class WaktuKerja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'waktu_kerja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip', 'jam_awal', 'jam_pulang'], 'required'],
            [['jam_awal', 'jam_pulang'], 'safe'],
            [['deskripsi'], 'string'],
            [['is_deleted'], 'integer'],
            [['nip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nip' => 'Nip',
            'jam_awal' => 'Jam Awal',
            'jam_pulang' => 'Jam Pulang',
            'deskripsi' => 'Deskripsi',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
