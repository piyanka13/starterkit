<?php

namespace backend\models;

use Yii;


/**
 * This is the model class for table "karyawan".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $id_posisi
 * @property integer $id_departemen
 * @property double $efektivitas
 * @property integer $is_deleted
 * @property integer $nip
 * @property Posisi $idPosisi
 * @property Departemen $idDepartemen
 * @property Pekerjaan[] $pekerjaans
 */
class Karyawan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'karyawan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'id_posisi', 'id_departemen'], 'required'],
            [['id_posisi', 'id_departemen', 'is_deleted','nip'], 'integer'],
            [['nama'], 'string', 'max' => 30],
            [['id_posisi'], 'exist', 'skipOnError' => true, 'targetClass' => Posisi::className(), 'targetAttribute' => ['id_posisi' => 'id']],
            [['id_departemen'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['id_departemen' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'id_posisi' => 'Posisi',
            'id_departemen' => 'Departemen',
            'is_deleted' => 'Is Deleted',
			'nip' =>'Nip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPosisi()
    {
        return $this->hasOne(Posisi::className(), ['id' => 'id_posisi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'id_departemen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaans()
    {
        return $this->hasMany(Pekerjaan::className(), ['id_karyawan' => 'id']);
    }
}
