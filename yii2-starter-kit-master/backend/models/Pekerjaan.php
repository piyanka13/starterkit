<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pekerjaan".
 *
 * @property integer $id
 * @property string $nama
 * @property string $deskripsi
 * @property string $waktu_mulai
 * @property string $waktu_selesai
 * @property string $nip
 * @property integer $is_finished
 * @property integer $is_validated
 * @property string $created_by
 */
class Pekerjaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pekerjaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'deskripsi', 'waktu_mulai', 'waktu_selesai', 'nip', 'is_validated', 'created_by'], 'required'],
            [['deskripsi'], 'string'],
            [['waktu_mulai', 'waktu_selesai'], 'safe'],
            [['is_finished', 'is_validated'], 'integer'],
            [['nama'], 'string', 'max' => 30],
            [['nip', 'created_by'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'nip' => 'Nip',
            'id' => 'ID',
            'nama' => 'Pekerjaan',
            'deskripsi' => 'Deskripsi',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'is_finished' => 'Is Finished',
            'is_validated' => 'Is Validated',
            'created_by' => 'Created By',
        ];
    }
}
