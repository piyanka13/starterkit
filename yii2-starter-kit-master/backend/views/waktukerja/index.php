<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WaktukerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Waktukerjas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="waktukerja-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Waktukerja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_karyawan',
            'date',
            'jam_awal',
            'jam_pulang',
            // 'efektifitas',
            // 'is_deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
