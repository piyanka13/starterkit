<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Waktukerja */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Waktukerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="waktukerja-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_karyawan',
            'date',
            'jam_awal',
            'jam_pulang',
            'efektifitas',
            'is_deleted',
        ],
    ]) ?>

</div>
