<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Waktukerja */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="waktukerja-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'id_karyawan')->textInput() ?>

    <?php echo $form->field($model, 'date')->textInput() ?>

    <?php echo $form->field($model, 'jam_awal')->textInput() ?>

    <?php echo $form->field($model, 'jam_pulang')->textInput() ?>

    <?php echo $form->field($model, 'efektifitas')->dropDownList([ 'Efektif' => 'Efektif', 'Tidak Efektif' => 'Tidak Efektif', ], ['prompt' => '']) ?>

    <?php echo $form->field($model, 'is_deleted')->textInput() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
