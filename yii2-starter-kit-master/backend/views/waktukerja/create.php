<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Waktukerja */

$this->title = 'Create Waktukerja';
$this->params['breadcrumbs'][] = ['label' => 'Waktukerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="waktukerja-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
