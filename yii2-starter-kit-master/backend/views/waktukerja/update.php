<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Waktukerja */

$this->title = 'Update Waktukerja: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Waktukerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="waktukerja-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
