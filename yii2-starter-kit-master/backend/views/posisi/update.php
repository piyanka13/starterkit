<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Posisi */

$this->title = 'Update Posisi: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Posisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
