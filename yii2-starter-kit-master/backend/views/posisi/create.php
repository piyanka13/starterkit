<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Posisi */

$this->title = 'Create Posisi';
$this->params['breadcrumbs'][] = ['label' => 'Posisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
