<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\WaktuKerja */

$this->title = 'Create Waktu Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Waktu Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="waktu-kerja-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
