<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

use kartik\datetime\DateTimePicker;

use backend\models\Karyawan;
/* @var $this yii\web\View */
/* @var $model backend\models\WaktuKerja */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="waktu-kerja-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>
	
	
    <?= //$form->field($model, 'id_karyawan')->textInput()
		Html::activeLabel($model, 'nip');
		echo Html::activeDropDownList($model, 'nip', ArrayHelper::map(Karyawan::find()->all(), 'id', 'nama'));
	?>
	
    <?= $form->field($model, 'jam_awal')->widget(DateTimePicker::className(), [
		'name' => 'datetime',
		'options' => ['placeholder' => 'waktu awal'],
		'convertFormat' => true,
		'pluginOptions' => [
			'format' => 'yyyy-MM-dd hh:mm:ss',
			'todayHighlight' => true
		]
    ])->label('Waktu Awal'); ?>
    <?= $form->field($model, 'jam_pulang')->widget(DateTimePicker::className(), [
		'name' => 'datetime',
		'options' => ['placeholder' => 'waktu akhir'],
		'convertFormat' => true,
		'pluginOptions' => [
			'format' => 'yyyy-MM-dd hh:mm:ss',
			'todayHighlight' => true
		]
    ])->label('Waktu Akhir'); ?>

    <?php echo $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_deleted')->hiddenInput(['value' => '0'])->label(false);?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
