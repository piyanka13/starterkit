<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\WaktuKerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Waktu Kerjas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="waktu-kerja-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Waktu Kerja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nip',
            'jam_awal',
            'jam_pulang',
            'deskripsi:ntext',
            // 'is_deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
