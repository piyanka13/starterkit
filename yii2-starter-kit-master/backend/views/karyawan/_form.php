<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Posisi;
use backend\models\Departemen;


/* @var $this yii\web\View */
/* @var $model app\models\Karyawan */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="karyawan-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="col-md-6">
    <?php echo $form->errorSummary($model); ?>
	
	
    <?php echo $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'id_posisi')->dropDownList(ArrayHelper::map(Posisi::find()->orderBy(['posisi'=>SORT_DESC])->all(),'id','posisi'))?>

    <?php echo $form->field($model, 'id_departemen')->dropDownList(ArrayHelper::map(Departemen::find()->orderBy(['nama'=>SORT_DESC])->all(),'id','nama')) ?>

	<?php echo $form->field($model, 'nip')->textInput() ?>
	
    <div class="form-group">
		<div class="col-sm-offset-9 col-sm-9">
			<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			 <?= Html::resetButton('Cancel', ['class' => 'btn btn-default']) ?>
		</div>
     
    </div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
