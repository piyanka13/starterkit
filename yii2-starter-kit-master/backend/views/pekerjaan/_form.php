<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

use kartik\datetime\DateTimePicker;

use backend\models\Karyawan;

/* @var $this yii\web\View */
/* @var $model backend\models\Pekerjaan */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="pekerjaan-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="col-md-6">
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'waktu_mulai')->widget(DateTimePicker::className(), [
		'name' => 'datetime',
		'options' => ['placeholder' => 'waktu mulai'],
		'convertFormat' => true,
		'pluginOptions' => [
			'format' => 'yyyy-MM-dd hh:mm:ss',
			'todayHighlight' => true
		]
    ]); ?>

    <?= $form->field($model, 'waktu_selesai')->widget(DateTimePicker::className(), [
		'name' => 'datetime',
		'options' => ['placeholder' => 'waktu selesai'],
		'convertFormat' => true,
		'pluginOptions' => [
			'format' => 'yyyy-MM-dd hh:mm:ss',
			'todayHighlight' => true
		]
    ]); ?>
	
    <?= //$form->field($model, 'nip')->textInput(),
		Html::activeLabel($model, 'nip');
		echo Html::activeDropDownList($model, 'nip', ArrayHelper::map(Karyawan::find()->all(), 'nip', 'nip'),['class' => 'form-control']);
    ?>

<<<<<<< HEAD
    <?php //$form->field($model, 'is_finished')->checkBox();	?>
	
	<?php //$form->field($model, 'is_validated')->checkBox(); ?>
=======
    <?= $form->field($model, 'is_finished')->hiddenInput(['value'=>'0'])->label(false) ?>
	
	<?= $form->field($model, 'is_validated')->hiddenInput(['value'=>'0'])->label(false) ?>
>>>>>>> e48f275bd36cfc2b4e9a928f34b792853c054799
	
	<?=
	 $form->field($model, 'created_by')->hiddenInput(['value' => Yii::$app->user->identity->nip ])->label(false) ?>
	
    <div class="form-group">
		<div class="col-sm-offset-9 col-sm-9">
			<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			 <?= Html::resetButton('Cancel', ['class' => 'btn btn-default']) ?>
		</div>
	</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
