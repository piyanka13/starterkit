<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Pekerjaan */

$this->title = 'Create Pekerjaan';
$this->params['breadcrumbs'][] = ['label' => 'Pekerjaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pekerjaan-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
