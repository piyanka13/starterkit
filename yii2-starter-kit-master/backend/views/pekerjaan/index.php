<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PekerjaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pekerjaan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pekerjaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
			if (Yii::$app->user->can('manager')) {
				echo Html::a('Create Pekerjaan', ['create'], ['class' => 'btn btn-success']);
			}
			
			
			$modelUser = Yii::$app->user->identity;
			//var_dump($modelUser->email);
			
		
		?>
    </p>
	
	
    <?=
	
    GridView::widget([
	 
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
		

				//'id',
				'nip',
				'nama',
				'deskripsi:ntext',
				'waktu_mulai',
				'waktu_selesai',
				// 'id_karyawan',
				// 'is_finished',
				//'is_validated',
				'created_by',
				
				[
					'label' => 'Status',
					'value' => function($model) {
						if(Yii::$app->user->can('user')){
							if($model->is_finished == 1){
								return Html::button('sudah Selesai ' ,['class' => 'btn btn-success']);
							}else{
								return Html::a('klik jika Selesai ' ,['selesai','id' => $model->id], 
								[
								'class' => 'btn btn-primary',
								'data' => [
									'confirm' => Yii::t('backend', 'Apakah benar pekerjaan ini telah selesai?'),
									'method' => 'post',
								]
							]);
							}
						}else{
							if($model->is_finished == 1)
								return Html::button('sudah Selesai ' ,['class' => 'btn btn-success']);
							else
								return Html::button('belum Selesai ' ,['class' => 'btn btn-warning']);
								
						}
					},
					'format' => 'raw',
				],
							[
					'label' => 'Validasi',
					'value' => function($model) {
						if(Yii::$app->user->can('manager')){
							if($model->is_validated == 1){
								return Html::button('sudah tervalidasi ' ,['class' => 'btn btn-danger']);
							}else{
								if($model->is_finished == 1){
									return Html::a('klik untuk menvalidasi ' ,['validasi','id' => $model->id], 
									[
									'class' => 'btn btn-primary',
									'data' => [
										'confirm' => Yii::t('backend', 'Apakah anda ingin memvalidasi tugas ini?'),
										'method' => 'post',
									]]);
								}else{
									return 'belum dapat divalidasi';
								}
							}
						}else{
							if($model->is_validated == 1)
								return Html::button('sudah tervalidasi ' ,['class' => 'btn btn-danger']);
							else
								return 'belum divalidasi';
						}
					},
					'format' => 'raw',
				],
			   // ['class' => 'yii\grid\CheckBoxColumn'],
        ],
    ]); ?>
	
	<?php

    $this->registerJs('
        var gridview_id = ""; 
        var columns = [2]; 
 
 
        var column_data = [];
            column_start = [];
            rowspan = [];
 
        for (var i = 0; i < columns.length; i++) {
            column = columns[i];
            column_data[column] = "";
            column_start[column] = null;
            rowspan[column] = 1;
        }
 
        var row = 1;
        $(gridview_id+" table > tbody  > tr").each(function() {
            var col = 1;
            $(this).find("td").each(function(){
                for (var i = 0; i < columns.length; i++) {
                    if(col==columns[i]){
                        if(column_data[columns[i]] == $(this).html()){
                            $(this).remove();
                            rowspan[columns[i]]++;
                            $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                        }
                        else{
                            column_data[columns[i]] = $(this).html();
                            rowspan[columns[i]] = 1;
                            column_start[columns[i]] = $(this);
                        }
                    }
                }
                col++;
            })
            row++;
        });
    ');
    ?>
</div>
