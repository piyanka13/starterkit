<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PekerjaanSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="pekerjaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'nama') ?>

    <?php echo $form->field($model, 'deskripsi') ?>

    <?php echo $form->field($model, 'waktu_mulai') ?>

    <?php echo $form->field($model, 'waktu_selesai') ?>

    <?php // echo $form->field($model, 'nip') ?>

    <?php // echo $form->field($model, 'is_finished') ?>

    <?php // echo $form->field($model, 'is_validated') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
