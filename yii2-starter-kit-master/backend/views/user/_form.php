<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">
	
    <?php $form = ActiveForm::begin(); ?>
	<div class="col-md-6">
        <?php echo $form->field($model, 'username') ?>
        <?php echo $form->field($model, 'email') ?>
		<?php echo $form->field($model, 'nip')-> textInput() ?>
        <?php echo $form->field($model, 'password')->passwordInput() ?>
        <?php echo $form->field($model, 'status')->dropDownList(User::statuses()) ?>
        <?php echo $form->field($model, 'roles')->checkboxList($roles) ?>
        <div class="form-group">
			<div class="col-sm-offset-9 col-sm-9">
			
			 <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
			 
			 <?= Html::resetButton('Cancel', ['class' => 'btn btn-default']) ?>
			 
			 </div>
        </div>
	
	</div>
    <?php ActiveForm::end(); ?>

</div>
