<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Departemen */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="departemen-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="col-md-6">
    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
		<div class="col-sm-offset-9 col-sm-9">
			<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			 <?= Html::resetButton('Cancel', ['class' => 'btn btn-default']) ?>
		</div>
     
    </div>
	
	</div>
    <?php ActiveForm::end(); ?>

</div>
