<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\datetime\DateTimePicker;

use frontend\models\Karyawan;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pekerjaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pekerjaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'waktu_mulai')->widget(DateTimePicker::className(), [
		'name' => 'datetime',
		'options' => ['placeholder' => 'waktu mulai'],
		'convertFormat' => true,
		'pluginOptions' => [
			'format' => 'yyyy-MM-dd hh:mm:ss',
			'todayHighlight' => true
		]
    ]); ?>

    <?= $form->field($model, 'waktu_selesai')->widget(DateTimePicker::className(), [
		'name' => 'datetime',
		'options' => ['placeholder' => 'waktu selesai'],
		'convertFormat' => true,
		'pluginOptions' => [
			'format' => 'yyyy-MM-dd hh:mm:ss',
			'todayHighlight' => true
		]
    ]); ?>

    <?= //$form->field($model, 'id_karyawan')->textInput()
		Html::activeLabel($model, 'id_karyawan');
		echo Html::activeDropDownList($model, 'id_karyawan', ArrayHelper::map(Karyawan::find()->all(), 'id', 'id'));
	?>

    <?= $form->field($model, 'is_finished')->checkBox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
