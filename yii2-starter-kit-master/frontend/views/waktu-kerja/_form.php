<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\time\TimePicker;

use frontend\models\Hari;

/* @var $this yii\web\View */
/* @var $model frontend\models\WaktuKerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="waktu-kerja-form">

    <?php $form = ActiveForm::begin(); ?>

 
    <?= 
		Html::activeLabel($model, 'hari');
		echo Html::activeDropDownList($model, 'day', ArrayHelper::map(Hari::find()->all(), 'id', 'nama'))?>

    <?= $form->field($model, 'jam_awal')->widget(TimePicker::className(),[]); ?>

    <?= $form->field($model, 'jam_pulang')->widget(TimePicker::className(), []); ?>
    
    <?= $form->field($model, 'deskripsi')->textarea(['rows'=>6]); ?>


    <?= $form->field($model, 'is_deleted')->hiddenInput(['value' => '0'])->label(false);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
