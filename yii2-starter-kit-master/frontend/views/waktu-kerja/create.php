<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\WaktuKerja */

$this->title = 'Create Waktu Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Waktu Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="waktu-kerja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
