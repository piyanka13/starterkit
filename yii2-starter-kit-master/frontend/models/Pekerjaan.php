<?php

namespace backend\models;
use Yii;

/**
 * This is the model class for table "pekerjaan".
 *
 * @property integer $id
 * @property string $nama
 * @property string $deskripsi
 * @property string $waktu_mulai
 * @property string $waktu_selesai
 * @property integer $id_karyawan
 * @property integer $is_finished
 */
class Pekerjaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pekerjaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'deskripsi', 'waktu_mulai', 'waktu_selesai', 'id_karyawan'], 'required'],
            [['deskripsi'], 'string'],
            [['waktu_mulai', 'waktu_selesai'], 'safe'],
            [['id_karyawan', 'is_finished'], 'integer'],
            [['nama'], 'string', 'max' => 30],
            [['id_karyawan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_karyawan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'id_karyawan' => 'Id Karyawan',
            'is_finished' => 'Is Finished',
        ];
    }
}
