<?php

namespace backend\models;
use Yii;

/**
 * This is the model class for table "posisi".
 *
 * @property integer $id
 * @property string $posisi
 * @property integer $is_deleted
 */
class Posisi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posisi'], 'required'],
            [['is_deleted'], 'integer'],
            [['posisi'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'posisi' => 'Posisi',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
