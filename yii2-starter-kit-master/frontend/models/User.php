<?php

namespace backend\models;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $id_karyawan
 * @property string $otoritas
 * @property integer $is_deleted
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'id_karyawan', 'otoritas'], 'required'],
            [['id_karyawan', 'is_deleted'], 'integer'],
            [['otoritas'], 'string'],
            [['username'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 50],
            [['id_karyawan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_karyawan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'id_karyawan' => 'Id Karyawan',
            'otoritas' => 'Otoritas',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
