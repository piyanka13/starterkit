-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2016 at 04:36 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `slug` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `thumbnail_base_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `published_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_attachment`
--

CREATE TABLE `article_attachment` (
  `id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `base_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `id` int(11) NOT NULL,
  `slug` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_category`
--

INSERT INTO `article_category` (`id`, `slug`, `title`, `body`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'news', 'News', NULL, NULL, 1, 1475243969, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE `departemen` (
  `id` int(4) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`id`, `nama`, `is_deleted`) VALUES
(1, 'Keuangan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `file_storage_item`
--

CREATE TABLE `file_storage_item` (
  `id` int(11) NOT NULL,
  `component` varchar(255) NOT NULL,
  `base_url` varchar(1024) NOT NULL,
  `path` varchar(1024) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE `hari` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hari`
--

INSERT INTO `hari` (`id`, `nama`) VALUES
(1, 'Minggu'),
(2, 'Senin'),
(3, 'Selasa'),
(4, 'Rabu'),
(5, 'Kamis'),
(6, 'Jumat'),
(7, 'Sabtu');

-- --------------------------------------------------------

--
-- Table structure for table `i18n_message`
--

CREATE TABLE `i18n_message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `i18n_source_message`
--

CREATE TABLE `i18n_source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(4) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `id_posisi` int(4) NOT NULL,
  `id_departemen` int(4) NOT NULL,
  `efektivitas` float NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `nip` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `id_posisi`, `id_departemen`, `efektivitas`, `is_deleted`, `nip`) VALUES
(2, '1', 1, 1, 1, 1, ''),
(3, '3', 1, 1, 3, 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `key_storage_item`
--

CREATE TABLE `key_storage_item` (
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `key_storage_item`
--

INSERT INTO `key_storage_item` (`key`, `value`, `comment`, `updated_at`, `created_at`) VALUES
('backend.layout-boxed', '0', NULL, NULL, NULL),
('backend.layout-collapsed-sidebar', '0', NULL, NULL, NULL),
('backend.layout-fixed', '0', NULL, NULL, NULL),
('backend.theme-skin', 'skin-blue', 'skin-blue, skin-black, skin-purple, skin-green, skin-red, skin-yellow', NULL, NULL),
('frontend.maintenance', 'disabled', 'Set it to "true" to turn on maintenance mode', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `slug` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `slug`, `title`, `body`, `view`, `status`, `created_at`, `updated_at`) VALUES
(1, 'about', 'About', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 1, 1475243968, 1475243968);

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id` int(4) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `nip` int(4) NOT NULL,
  `is_finished` tinyint(1) NOT NULL DEFAULT '0',
  `is_validated` tinyint(1) NOT NULL,
  `created_by` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `nama`, `deskripsi`, `waktu_mulai`, `waktu_selesai`, `nip`, `is_finished`, `is_validated`, `created_by`) VALUES
(3, ' jbjhbjhbjhbjhb', ' kj,nk,n;k.jnkj.,nkjnjk', '2016-10-05 00:00:00', '2016-10-14 06:00:00', 2, 1, 0, ''),
(4, 'vfslvdmslv', 'sdflvnslkfsdlv', '2016-10-12 00:00:00', '2016-10-29 00:00:00', 3, 1, 0, ''),
(5, 'ngepel', 'membersihkan lantai dengan cara dipel', '2016-10-05 12:00:43', '2016-10-06 12:00:44', 2, 1, 0, ''),
(6, 'junianto', 'sehat dan sangatsubur', '2016-09-29 06:30:04', '2016-10-15 11:55:04', 2, 1, 0, ''),
(7, 'juni', 'opfljepwofjpi', '2016-10-05 12:50:05', '2016-10-05 06:30:06', 2, 1, 0, ''),
(8, 'sfclaskvmals', 'LNL;C.DNSFVSDNL;', '2016-10-05 12:55:13', '2016-10-13 02:55:13', 3, 1, 0, ''),
(9, 'juni`o;aljfoeilj', 'l;svkjs;emodflgjwdofslkjm;', '2016-10-05 10:50:42', '2016-10-05 05:25:42', 2, 1, 0, ''),
(10, 'ciadkjhciuqk', 'nsskejfhwej', '2016-10-06 08:10:05', '2016-10-15 11:55:04', 2, 1, 0, ''),
(11, 'cobacoba', 'ashdfukjeahsfkdawjpfo;sdaijvposakwapsofijo3pq', '2016-10-06 06:30:37', '2016-10-07 07:35:37', 2, 1, 0, '2'),
(12, '123', '123', '2016-10-12 01:45:57', '2016-09-29 10:30:57', 3, 0, 0, '100'),
(13, '124', '124', '2016-10-18 03:30:21', '2016-09-29 06:30:21', 2, 0, 0, '101'),
(14, '125', '125', '2016-10-06 06:35:54', '2016-09-27 05:30:54', 2, 0, 0, '100'),
(15, '125', '125', '2016-10-06 06:35:54', '2016-09-27 05:30:54', 2, 0, 0, '100');

-- --------------------------------------------------------

--
-- Table structure for table `posisi`
--

CREATE TABLE `posisi` (
  `id` int(4) NOT NULL,
  `posisi` varchar(20) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posisi`
--

INSERT INTO `posisi` (`id`, `posisi`, `is_deleted`) VALUES
(1, 'Manager', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rbac_auth_assignment`
--

CREATE TABLE `rbac_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rbac_auth_assignment`
--

INSERT INTO `rbac_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '1', 1475243992),
('manager', '2', 1475243992),
('manager', '4', 1475849675),
('user', '3', 1475243992);

-- --------------------------------------------------------

--
-- Table structure for table `rbac_auth_item`
--

CREATE TABLE `rbac_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rbac_auth_item`
--

INSERT INTO `rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, NULL, NULL, NULL, 1475243992, 1475243992),
('editOwnModel', 2, NULL, 'ownModelRule', NULL, 1475243992, 1475243992),
('loginToBackend', 2, NULL, NULL, NULL, 1475243992, 1475243992),
('manager', 1, NULL, NULL, NULL, 1475243992, 1475243992),
('user', 1, NULL, NULL, NULL, 1475243992, 1475243992);

-- --------------------------------------------------------

--
-- Table structure for table `rbac_auth_item_child`
--

CREATE TABLE `rbac_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rbac_auth_item_child`
--

INSERT INTO `rbac_auth_item_child` (`parent`, `child`) VALUES
('administrator', 'loginToBackend'),
('manager', 'loginToBackend'),
('user', 'editOwnModel'),
('user', 'loginToBackend');

-- --------------------------------------------------------

--
-- Table structure for table `rbac_auth_rule`
--

CREATE TABLE `rbac_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rbac_auth_rule`
--

INSERT INTO `rbac_auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('ownModelRule', 'O:29:"common\\rbac\\rule\\OwnModelRule":3:{s:4:"name";s:12:"ownModelRule";s:9:"createdAt";i:1475243992;s:9:"updatedAt";i:1475243992;}', 1475243992, 1475243992);

-- --------------------------------------------------------

--
-- Table structure for table `system_db_migration`
--

CREATE TABLE `system_db_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_db_migration`
--

INSERT INTO `system_db_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1475243863),
('m140703_123000_user', 1475243951),
('m140703_123055_log', 1475243952),
('m140703_123104_page', 1475243952),
('m140703_123803_article', 1475243958),
('m140703_123813_rbac', 1475243959),
('m140709_173306_widget_menu', 1475243960),
('m140709_173333_widget_text', 1475243960),
('m140712_123329_widget_carousel', 1475243961),
('m140805_084745_key_storage_item', 1475243963),
('m141012_101932_i18n_tables', 1475243965),
('m150318_213934_file_storage_item', 1475243965),
('m150414_195800_timeline_event', 1475243966),
('m150725_192740_seed_data', 1475243969),
('m150929_074021_article_attachment_order', 1475243970),
('m160203_095604_user_token', 1475243970);

-- --------------------------------------------------------

--
-- Table structure for table `system_log`
--

CREATE TABLE `system_log` (
  `id` bigint(20) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_log`
--

INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
(107, 1, 'yii\\base\\ErrorException:64', 1475596919.2552, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(108, 1, 'yii\\base\\ErrorException:64', 1475597047.3185, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(109, 1, 'yii\\base\\ErrorException:64', 1475597063.4035, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(110, 1, 'yii\\base\\ErrorException:64', 1475598272.0346, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(111, 1, 'yii\\base\\ErrorException:64', 1475598357.3635, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(112, 1, 'yii\\base\\ErrorException:64', 1475598657.7757, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(113, 1, 'yii\\base\\ErrorException:64', 1475598658.8057, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(114, 1, 'yii\\base\\ErrorException:64', 1475598664.9771, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(115, 1, 'yii\\base\\ErrorException:64', 1475598749.5979, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(116, 1, 'yii\\base\\ErrorException:64', 1475598997.1391, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(117, 1, 'yii\\base\\ErrorException:4', 1475599861.7805, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''=>'' (T_DOUBLE_ARROW)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:45\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(118, 1, 'yii\\base\\UnknownPropertyException', 1475600433.7182, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/selesai?id=3]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(266): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(252): yii\\validators\\Validator->validateAttribute(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\Pekerjaan), Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php(527): yii\\base\\Model->validate(NULL)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(600): yii\\db\\ActiveRecord->update(true, NULL)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\db\\BaseActiveRecord->save()\n#7 [internal function]: backend\\controllers\\PekerjaanController->actionSelesai(''3'')\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''selesai'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/seles...'', Array)\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#14 {main}'),
(119, 1, 'yii\\base\\UnknownPropertyException', 1475600473.1445, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::ID'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''ID'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(46): yii\\db\\BaseActiveRecord->__get(''ID'')\n#2 [internal function]: yii\\base\\View->{closure}(Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(211): call_user_func(Object(Closure), Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(225): yii\\grid\\DataColumn->getDataCellValue(Object(backend\\models\\Pekerjaan), 3, 0)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\Column.php(108): yii\\grid\\DataColumn->renderDataCellContent(Object(backend\\models\\Pekerjaan), 3, 0)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(504): yii\\grid\\Column->renderDataCell(Object(backend\\models\\Pekerjaan), 3, 0)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(473): yii\\grid\\GridView->renderTableRow(Object(backend\\models\\Pekerjaan), 3, 0)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(344): yii\\grid\\GridView->renderTableBody()\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(151): yii\\grid\\GridView->renderItems()\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(313): yii\\widgets\\BaseListView->renderSection(''{items}'')\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(126): yii\\grid\\GridView->renderSection(''{items}'')\n#12 [internal function]: yii\\widgets\\BaseListView->yii\\widgets\\{closure}(Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(129): preg_replace_callback(''/{\\\\w+}/'', Object(Closure), ''{summary}\\n{item...'')\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(288): yii\\widgets\\BaseListView->run()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(102): yii\\grid\\GridView->run()\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(52): yii\\base\\Widget::widget(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''index'', Array, Object(backend\\controllers\\PekerjaanController))\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(41): yii\\base\\Controller->render(''index'', Array)\n#22 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#23 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#25 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#26 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#27 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#28 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#29 {main}'),
(120, 1, 'yii\\base\\UnknownPropertyException', 1475600476.4967, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::ID'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''ID'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(46): yii\\db\\BaseActiveRecord->__get(''ID'')\n#2 [internal function]: yii\\base\\View->{closure}(Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(211): call_user_func(Object(Closure), Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(225): yii\\grid\\DataColumn->getDataCellValue(Object(backend\\models\\Pekerjaan), 3, 0)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\Column.php(108): yii\\grid\\DataColumn->renderDataCellContent(Object(backend\\models\\Pekerjaan), 3, 0)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(504): yii\\grid\\Column->renderDataCell(Object(backend\\models\\Pekerjaan), 3, 0)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(473): yii\\grid\\GridView->renderTableRow(Object(backend\\models\\Pekerjaan), 3, 0)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(344): yii\\grid\\GridView->renderTableBody()\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(151): yii\\grid\\GridView->renderItems()\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(313): yii\\widgets\\BaseListView->renderSection(''{items}'')\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(126): yii\\grid\\GridView->renderSection(''{items}'')\n#12 [internal function]: yii\\widgets\\BaseListView->yii\\widgets\\{closure}(Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(129): preg_replace_callback(''/{\\\\w+}/'', Object(Closure), ''{summary}\\n{item...'')\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(288): yii\\widgets\\BaseListView->run()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(102): yii\\grid\\GridView->run()\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(52): yii\\base\\Widget::widget(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''index'', Array, Object(backend\\controllers\\PekerjaanController))\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(41): yii\\base\\Controller->render(''index'', Array)\n#22 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#23 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#25 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#26 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#27 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#28 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#29 {main}'),
(121, 1, 'yii\\base\\UnknownPropertyException', 1475600480.1609, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::ID'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''ID'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(46): yii\\db\\BaseActiveRecord->__get(''ID'')\n#2 [internal function]: yii\\base\\View->{closure}(Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(211): call_user_func(Object(Closure), Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(225): yii\\grid\\DataColumn->getDataCellValue(Object(backend\\models\\Pekerjaan), 3, 0)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\Column.php(108): yii\\grid\\DataColumn->renderDataCellContent(Object(backend\\models\\Pekerjaan), 3, 0)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(504): yii\\grid\\Column->renderDataCell(Object(backend\\models\\Pekerjaan), 3, 0)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(473): yii\\grid\\GridView->renderTableRow(Object(backend\\models\\Pekerjaan), 3, 0)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(344): yii\\grid\\GridView->renderTableBody()\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(151): yii\\grid\\GridView->renderItems()\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(313): yii\\widgets\\BaseListView->renderSection(''{items}'')\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(126): yii\\grid\\GridView->renderSection(''{items}'')\n#12 [internal function]: yii\\widgets\\BaseListView->yii\\widgets\\{closure}(Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(129): preg_replace_callback(''/{\\\\w+}/'', Object(Closure), ''{summary}\\n{item...'')\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(288): yii\\widgets\\BaseListView->run()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(102): yii\\grid\\GridView->run()\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(52): yii\\base\\Widget::widget(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''index'', Array, Object(backend\\controllers\\PekerjaanController))\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(41): yii\\base\\Controller->render(''index'', Array)\n#22 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#23 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#25 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#26 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#27 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#28 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#29 {main}'),
(122, 1, 'yii\\base\\UnknownPropertyException', 1475600526.6135, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/selesai?no=3]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(266): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(252): yii\\validators\\Validator->validateAttribute(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\Pekerjaan), Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php(527): yii\\base\\Model->validate(NULL)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(600): yii\\db\\ActiveRecord->update(true, NULL)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\db\\BaseActiveRecord->save()\n#7 [internal function]: backend\\controllers\\PekerjaanController->actionSelesai(''3'')\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''selesai'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/seles...'', Array)\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#14 {main}'),
(123, 1, 'yii\\base\\UnknownPropertyException', 1475600743.856, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/selesai?id=3]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(266): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(252): yii\\validators\\Validator->validateAttribute(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\Pekerjaan), Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php(527): yii\\base\\Model->validate(NULL)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(600): yii\\db\\ActiveRecord->update(true, NULL)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\db\\BaseActiveRecord->save()\n#7 [internal function]: backend\\controllers\\PekerjaanController->actionSelesai(''3'')\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''selesai'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/seles...'', Array)\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#14 {main}'),
(124, 1, 'yii\\base\\UnknownClassException', 1475600941.0733, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownClassException'' with message ''Unable to find ''backend\\models\\Pekerjaan'' in file: C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend/models/Pekerjaan.php. Namespace missing?'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php:291\nStack trace:\n#0 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(14): spl_autoload_call(''backend\\\\models\\\\...'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(288): include(''C:\\\\xampp\\\\htdocs...'')\n#3 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(35): spl_autoload_call(''backend\\\\models\\\\...'')\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(125, 1, 'yii\\base\\UnknownClassException', 1475600959.8883, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownClassException'' with message ''Unable to find ''backend\\models\\Pekerjaan'' in file: C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend/models/Pekerjaan.php. Namespace missing?'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php:291\nStack trace:\n#0 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(14): spl_autoload_call(''backend\\\\models\\\\...'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(288): include(''C:\\\\xampp\\\\htdocs...'')\n#3 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(35): spl_autoload_call(''backend\\\\models\\\\...'')\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(126, 1, 'yii\\base\\UnknownClassException', 1475600962.7705, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownClassException'' with message ''Unable to find ''backend\\models\\Pekerjaan'' in file: C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend/models/Pekerjaan.php. Namespace missing?'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php:291\nStack trace:\n#0 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(14): spl_autoload_call(''backend\\\\models\\\\...'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(288): include(''C:\\\\xampp\\\\htdocs...'')\n#3 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(35): spl_autoload_call(''backend\\\\models\\\\...'')\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(127, 1, 'yii\\base\\UnknownClassException', 1475601006.187, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan]', 'exception ''yii\\base\\UnknownClassException'' with message ''Unable to find ''backend\\models\\Pekerjaan'' in file: C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend/models/Pekerjaan.php. Namespace missing?'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php:291\nStack trace:\n#0 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(14): spl_autoload_call(''backend\\\\models\\\\...'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(288): include(''C:\\\\xampp\\\\htdocs...'')\n#3 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(35): spl_autoload_call(''backend\\\\models\\\\...'')\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction('''', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(128, 1, 'yii\\base\\ErrorException:8', 1475601483.6853, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''Undefined offset: 0'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseUrl.php:98\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseUrl.php(98): yii\\base\\ErrorHandler->handleError(8, ''Undefined offse...'', ''C:\\\\xampp\\\\htdocs...'', 98, Array)\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseUrl.php(209): yii\\helpers\\BaseUrl::toRoute(Array, false)\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(402): yii\\helpers\\BaseUrl::to(Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(48): yii\\helpers\\BaseHtml::a(''sudah selesai S...'', Array)\n#4 [internal function]: yii\\base\\View->{closure}(Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(211): call_user_func(Object(Closure), Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(225): yii\\grid\\DataColumn->getDataCellValue(Object(backend\\models\\Pekerjaan), 3, 0)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\Column.php(108): yii\\grid\\DataColumn->renderDataCellContent(Object(backend\\models\\Pekerjaan), 3, 0)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(504): yii\\grid\\Column->renderDataCell(Object(backend\\models\\Pekerjaan), 3, 0)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(473): yii\\grid\\GridView->renderTableRow(Object(backend\\models\\Pekerjaan), 3, 0)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(344): yii\\grid\\GridView->renderTableBody()\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(151): yii\\grid\\GridView->renderItems()\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(313): yii\\widgets\\BaseListView->renderSection(''{items}'')\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(126): yii\\grid\\GridView->renderSection(''{items}'')\n#14 [internal function]: yii\\widgets\\BaseListView->yii\\widgets\\{closure}(Array)\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(129): preg_replace_callback(''/{\\\\w+}/'', Object(Closure), ''{summary}\\n{item...'')\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(288): yii\\widgets\\BaseListView->run()\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(102): yii\\grid\\GridView->run()\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(56): yii\\base\\Widget::widget(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''index'', Array, Object(backend\\controllers\\PekerjaanController))\n#23 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(41): yii\\base\\Controller->render(''index'', Array)\n#24 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#25 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#26 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#27 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#28 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#29 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#30 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#31 {main}'),
(129, 1, 'yii\\base\\ErrorException:4', 1475601501.1473, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''=>'' (T_DOUBLE_ARROW)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:45\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(130, 1, 'yii\\base\\ErrorException:8', 1475601555.9304, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''Undefined offset: 0'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseUrl.php:98\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseUrl.php(98): yii\\base\\ErrorHandler->handleError(8, ''Undefined offse...'', ''C:\\\\xampp\\\\htdocs...'', 98, Array)\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseUrl.php(209): yii\\helpers\\BaseUrl::toRoute(Array, false)\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(402): yii\\helpers\\BaseUrl::to(Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(46): yii\\helpers\\BaseHtml::a(''sudah selesai S...'', Array)\n#4 [internal function]: yii\\base\\View->{closure}(Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(211): call_user_func(Object(Closure), Object(backend\\models\\Pekerjaan), 3, 0, Object(yii\\grid\\DataColumn))\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(225): yii\\grid\\DataColumn->getDataCellValue(Object(backend\\models\\Pekerjaan), 3, 0)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\Column.php(108): yii\\grid\\DataColumn->renderDataCellContent(Object(backend\\models\\Pekerjaan), 3, 0)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(504): yii\\grid\\Column->renderDataCell(Object(backend\\models\\Pekerjaan), 3, 0)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(473): yii\\grid\\GridView->renderTableRow(Object(backend\\models\\Pekerjaan), 3, 0)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(344): yii\\grid\\GridView->renderTableBody()\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(151): yii\\grid\\GridView->renderItems()\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(313): yii\\widgets\\BaseListView->renderSection(''{items}'')\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(126): yii\\grid\\GridView->renderSection(''{items}'')\n#14 [internal function]: yii\\widgets\\BaseListView->yii\\widgets\\{closure}(Array)\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(129): preg_replace_callback(''/{\\\\w+}/'', Object(Closure), ''{summary}\\n{item...'')\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(288): yii\\widgets\\BaseListView->run()\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(102): yii\\grid\\GridView->run()\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(56): yii\\base\\Widget::widget(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''index'', Array, Object(backend\\controllers\\PekerjaanController))\n#23 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(41): yii\\base\\Controller->render(''index'', Array)\n#24 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#25 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#26 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#27 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#28 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#29 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#30 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#31 {main}'),
(131, 1, 'yii\\base\\ErrorException:64', 1475601812.3401, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Cannot redeclare class backend\\models\\Karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\frontend\\models\\Karyawan.php:93\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}');
INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
(132, 1, 'yii\\base\\UnknownPropertyException', 1475601854.5415, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1651): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1495): yii\\helpers\\BaseHtml::activeListInput(''dropDownList'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array, Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(46): yii\\helpers\\BaseHtml::activeDropDownList(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(19): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(70): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(133, 1, 'yii\\base\\UnknownPropertyException', 1475601862.0449, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1651): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1495): yii\\helpers\\BaseHtml::activeListInput(''dropDownList'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array, Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(46): yii\\helpers\\BaseHtml::activeDropDownList(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(19): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(70): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(134, 1, 'yii\\base\\UnknownPropertyException', 1475601947.4028, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1651): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1495): yii\\helpers\\BaseHtml::activeListInput(''dropDownList'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array, Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(46): yii\\helpers\\BaseHtml::activeDropDownList(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(70): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(135, 1, 'yii\\base\\UnknownPropertyException', 1475601955.7813, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1651): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1495): yii\\helpers\\BaseHtml::activeListInput(''dropDownList'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array, Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(46): yii\\helpers\\BaseHtml::activeDropDownList(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(70): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(136, 1, 'yii\\base\\ErrorException:4', 1475602490.5629, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''='', expecting '']'''' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:58\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(137, 1, 'yii\\base\\ErrorException:4', 1475602542.1468, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''=>'' (T_DOUBLE_ARROW)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:59\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(138, 1, 'yii\\base\\UnknownPropertyException', 1475602562.871, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/selesai?id=3]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(266): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(252): yii\\validators\\Validator->validateAttribute(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\Pekerjaan), Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php(527): yii\\base\\Model->validate(NULL)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(600): yii\\db\\ActiveRecord->update(true, NULL)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(60): yii\\db\\BaseActiveRecord->save()\n#7 [internal function]: backend\\controllers\\PekerjaanController->actionSelesai(''3'')\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''selesai'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/seles...'', Array)\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#14 {main}'),
(139, 1, 'yii\\base\\UnknownPropertyException', 1475602814.4434, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1651): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1495): yii\\helpers\\BaseHtml::activeListInput(''dropDownList'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array, Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(46): yii\\helpers\\BaseHtml::activeDropDownList(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(78): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(140, 1, 'yii\\base\\UnknownPropertyException', 1475602854.7657, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1651): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1495): yii\\helpers\\BaseHtml::activeListInput(''dropDownList'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array, Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(46): yii\\helpers\\BaseHtml::activeDropDownList(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(78): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(141, 1, 'yii\\base\\ErrorException:4', 1475645527.0604, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''==='' (T_IS_IDENTICAL)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(142, 1, 'yii\\base\\ErrorException:4', 1475645534.9112, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''==='' (T_IS_IDENTICAL)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(143, 1, 'yii\\base\\ErrorException:4', 1475645612.8601, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''==='' (T_IS_IDENTICAL)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(144, 1, 'yii\\base\\ErrorException:4', 1475645618.0488, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''==='' (T_IS_IDENTICAL)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(145, 1, 'yii\\base\\ErrorException:4', 1475645630.7633, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''==='' (T_IS_IDENTICAL)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(146, 1, 'yii\\base\\ErrorException:4', 1475645651.6696, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''<'''' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(147, 1, 'yii\\base\\ErrorException:4', 1475645665.3442, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''Html'' (T_STRING), expecting '','' or '';'''' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:50\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(148, 1, 'yii\\base\\ErrorException:4', 1475645681.6467, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''>>'' (T_SR)'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:52\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(149, 1, 'yii\\base\\ErrorException:4', 1475645772.4574, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''Html'' (T_STRING), expecting '','' or '';'''' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:44\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(150, 1, 'yii\\base\\UnknownPropertyException', 1475645784.8892, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1234): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1283): yii\\helpers\\BaseHtml::activeInput(''text'', Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(367): yii\\helpers\\BaseHtml::activeTextInput(Object(backend\\models\\Pekerjaan), ''id_karyawan'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(43): yii\\widgets\\ActiveField->textInput()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(78): yii\\base\\Controller->render(''create'', Array)\n#15 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#16 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#22 {main}'),
(151, 1, 'yii\\base\\UnknownPropertyException', 1475646082.1879, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(266): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(252): yii\\validators\\Validator->validateAttribute(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\Pekerjaan), Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php(421): yii\\base\\Model->validate(NULL)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(598): yii\\db\\ActiveRecord->insert(true, NULL)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(73): yii\\db\\BaseActiveRecord->save()\n#7 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#14 {main}'),
(152, 1, 'yii\\base\\UnknownPropertyException', 1475646166.4008, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/view?id=5]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseArrayHelper.php(204): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(215): yii\\helpers\\BaseArrayHelper::getValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(124): yii\\widgets\\DetailView->normalizeAttributes()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Object.php(107): yii\\widgets\\DetailView->init()\n#5 [internal function]: yii\\base\\Object->__construct(Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(379): ReflectionClass->newInstanceArgs(Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(154): yii\\di\\Container->build(''yii\\\\widgets\\\\Det...'', Array, Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(344): yii\\di\\Container->get(''yii\\\\widgets\\\\Det...'', Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(101): yii\\BaseYii::createObject(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\view.php(37): yii\\base\\Widget::widget(Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''view'', Array, Object(backend\\controllers\\PekerjaanController))\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(53): yii\\base\\Controller->render(''view'', Array)\n#16 [internal function]: backend\\controllers\\PekerjaanController->actionView(''5'')\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''view'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/view'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#23 {main}'),
(153, 1, 'yii\\base\\UnknownPropertyException', 1475646325.7373, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/view?id=6]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseArrayHelper.php(204): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(215): yii\\helpers\\BaseArrayHelper::getValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(124): yii\\widgets\\DetailView->normalizeAttributes()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Object.php(107): yii\\widgets\\DetailView->init()\n#5 [internal function]: yii\\base\\Object->__construct(Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(379): ReflectionClass->newInstanceArgs(Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(154): yii\\di\\Container->build(''yii\\\\widgets\\\\Det...'', Array, Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(344): yii\\di\\Container->get(''yii\\\\widgets\\\\Det...'', Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(101): yii\\BaseYii::createObject(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\view.php(37): yii\\base\\Widget::widget(Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''view'', Array, Object(backend\\controllers\\PekerjaanController))\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(53): yii\\base\\Controller->render(''view'', Array)\n#16 [internal function]: backend\\controllers\\PekerjaanController->actionView(''6'')\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''view'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/view'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#23 {main}'),
(154, 1, 'yii\\base\\UnknownPropertyException', 1475646761.2039, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/view?id=6]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseArrayHelper.php(204): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(215): yii\\helpers\\BaseArrayHelper::getValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(124): yii\\widgets\\DetailView->normalizeAttributes()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Object.php(107): yii\\widgets\\DetailView->init()\n#5 [internal function]: yii\\base\\Object->__construct(Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(379): ReflectionClass->newInstanceArgs(Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(154): yii\\di\\Container->build(''yii\\\\widgets\\\\Det...'', Array, Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(344): yii\\di\\Container->get(''yii\\\\widgets\\\\Det...'', Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(101): yii\\BaseYii::createObject(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\view.php(37): yii\\base\\Widget::widget(Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''view'', Array, Object(backend\\controllers\\PekerjaanController))\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(53): yii\\base\\Controller->render(''view'', Array)\n#16 [internal function]: backend\\controllers\\PekerjaanController->actionView(''6'')\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''view'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/view'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#23 {main}');
INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
(155, 1, 'yii\\base\\UnknownPropertyException', 1475646810.3586, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/view?id=7]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseArrayHelper.php(204): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(215): yii\\helpers\\BaseArrayHelper::getValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(124): yii\\widgets\\DetailView->normalizeAttributes()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Object.php(107): yii\\widgets\\DetailView->init()\n#5 [internal function]: yii\\base\\Object->__construct(Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(379): ReflectionClass->newInstanceArgs(Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(154): yii\\di\\Container->build(''yii\\\\widgets\\\\Det...'', Array, Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(344): yii\\di\\Container->get(''yii\\\\widgets\\\\Det...'', Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(101): yii\\BaseYii::createObject(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\view.php(37): yii\\base\\Widget::widget(Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''view'', Array, Object(backend\\controllers\\PekerjaanController))\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(53): yii\\base\\Controller->render(''view'', Array)\n#16 [internal function]: backend\\controllers\\PekerjaanController->actionView(''7'')\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''view'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/view'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#23 {main}'),
(156, 1, 'yii\\base\\UnknownPropertyException', 1475647047.6937, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/view?id=8]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseArrayHelper.php(204): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(215): yii\\helpers\\BaseArrayHelper::getValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(124): yii\\widgets\\DetailView->normalizeAttributes()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Object.php(107): yii\\widgets\\DetailView->init()\n#5 [internal function]: yii\\base\\Object->__construct(Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(379): ReflectionClass->newInstanceArgs(Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(154): yii\\di\\Container->build(''yii\\\\widgets\\\\Det...'', Array, Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(344): yii\\di\\Container->get(''yii\\\\widgets\\\\Det...'', Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(101): yii\\BaseYii::createObject(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\view.php(37): yii\\base\\Widget::widget(Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''view'', Array, Object(backend\\controllers\\PekerjaanController))\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(53): yii\\base\\Controller->render(''view'', Array)\n#16 [internal function]: backend\\controllers\\PekerjaanController->actionView(''8'')\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''view'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/view'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#23 {main}'),
(157, 1, 'yii\\base\\ErrorException:1', 1475647153.7616, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Class ''backend\\models\\Pekerjaan'' not found'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:71\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(158, 1, 'yii\\base\\ErrorException:1', 1475647263.665, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''Class ''backend\\models\\Pekerjaan'' not found'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php:14\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(159, 1, 'yii\\base\\UnknownPropertyException', 1475647798.0832, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/view?id=9]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\Pekerjaan::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseArrayHelper.php(204): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(215): yii\\helpers\\BaseArrayHelper::getValue(Object(backend\\models\\Pekerjaan), ''id_karyawan'')\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\DetailView.php(124): yii\\widgets\\DetailView->normalizeAttributes()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Object.php(107): yii\\widgets\\DetailView->init()\n#5 [internal function]: yii\\base\\Object->__construct(Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(379): ReflectionClass->newInstanceArgs(Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\di\\Container.php(154): yii\\di\\Container->build(''yii\\\\widgets\\\\Det...'', Array, Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(344): yii\\di\\Container->get(''yii\\\\widgets\\\\Det...'', Array, Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(101): yii\\BaseYii::createObject(Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\view.php(37): yii\\base\\Widget::widget(Array)\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#12 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#13 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#14 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''view'', Array, Object(backend\\controllers\\PekerjaanController))\n#15 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(53): yii\\base\\Controller->render(''view'', Array)\n#16 [internal function]: backend\\controllers\\PekerjaanController->actionView(''9'')\n#17 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#18 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#19 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''view'', Array)\n#20 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/view'', Array)\n#21 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#22 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#23 {main}'),
(160, 1, 'yii\\base\\UnknownPropertyException', 1475648047.7334, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index?PekerjaanSearch%5Bnama%5D=&PekerjaanSearch%5Bdeskripsi%5D=kj&PekerjaanSearch%5Bwaktu_mulai%5D=&PekerjaanSearch%5Bwaktu_selesai%5D=]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\search\\PekerjaanSearch::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(249): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\search\\PekerjaanSearch), Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(50): yii\\base\\Model->validate()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(36): backend\\models\\search\\PekerjaanSearch->search(Array)\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(161, 1, 'yii\\base\\ErrorException:1', 1475758195.47, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''Class ''backend\\controllers\\ActiveDataProvider'' not found'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:41\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(162, 1, 'yii\\base\\ErrorException:8', 1475758456.4658, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index?PekerjaanSearch%5Bnama%5D=jk&PekerjaanSearch%5Bdeskripsi%5D=&PekerjaanSearch%5Bwaktu_mulai%5D=&PekerjaanSearch%5Bwaktu_selesai%5D=]', 'exception ''yii\\base\\ErrorException'' with message ''Undefined variable: dataProvider'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:51\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(51): yii\\base\\ErrorHandler->handleError(8, ''Undefined varia...'', ''C:\\\\xampp\\\\htdocs...'', 51, Array)\n#1 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#8 {main}'),
(163, 1, 'yii\\base\\UnknownPropertyException', 1475758532.623, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index?PekerjaanSearch%5Bnama%5D=jk&PekerjaanSearch%5Bdeskripsi%5D=&PekerjaanSearch%5Bwaktu_mulai%5D=&PekerjaanSearch%5Bwaktu_selesai%5D=]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\search\\PekerjaanSearch::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(249): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\search\\PekerjaanSearch), Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(50): yii\\base\\Model->validate()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(42): backend\\models\\search\\PekerjaanSearch->search(Array)\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(164, 1, 'yii\\base\\UnknownPropertyException', 1475758582.156, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index?PekerjaanSearch%5Bnama%5D=ekrjfn&PekerjaanSearch%5Bdeskripsi%5D=&PekerjaanSearch%5Bwaktu_mulai%5D=&PekerjaanSearch%5Bwaktu_selesai%5D=]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\search\\PekerjaanSearch::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(249): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\search\\PekerjaanSearch), Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(50): yii\\base\\Model->validate()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(42): backend\\models\\search\\PekerjaanSearch->search(Array)\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(165, 1, 'yii\\base\\UnknownPropertyException', 1475758598.5444, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index?PekerjaanSearch%5Bnama%5D=fve&PekerjaanSearch%5Bdeskripsi%5D=&PekerjaanSearch%5Bwaktu_mulai%5D=&PekerjaanSearch%5Bwaktu_selesai%5D=]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\search\\PekerjaanSearch::id_karyawan'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''id_karyawan'')\n#1 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\validators\\Validator.php(249): yii\\db\\BaseActiveRecord->__get(''id_karyawan'')\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Model.php(356): yii\\validators\\Validator->validateAttributes(Object(backend\\models\\search\\PekerjaanSearch), Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(50): yii\\base\\Model->validate()\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(42): backend\\models\\search\\PekerjaanSearch->search(Array)\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#9 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#10 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(166, 1, 'yii\\base\\ErrorException:8', 1475812395.2202, '[backend][/Starter/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''Undefined variable: dataProvider'' in C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:44\nStack trace:\n#0 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(44): yii\\base\\ErrorHandler->handleError(8, ''Undefined varia...'', ''C:\\\\xampp\\\\htdocs...'', 44, Array)\n#1 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#2 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#4 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#5 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#6 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#7 C:\\xampp\\htdocs\\Starter\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#8 {main}'),
(167, 1, 'yii\\base\\UnknownPropertyException', 1475848945.607, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: backend\\models\\search\\PekerjaanSearch::is_validated'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\db\\BaseActiveRecord.php(252): yii\\base\\Component->__get(''is_validated'')\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(2033): yii\\db\\BaseActiveRecord->__get(''is_validated'')\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1234): yii\\helpers\\BaseHtml::getAttributeValue(Object(backend\\models\\search\\PekerjaanSearch), ''is_validated'')\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1283): yii\\helpers\\BaseHtml::activeInput(''text'', Object(backend\\models\\search\\PekerjaanSearch), ''is_validated'', Array)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\DataColumn.php(191): yii\\helpers\\BaseHtml::activeTextInput(Object(backend\\models\\search\\PekerjaanSearch), ''is_validated'', Array)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\Column.php(116): yii\\grid\\DataColumn->renderFilterCellContent()\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(446): yii\\grid\\Column->renderFilterCell()\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(411): yii\\grid\\GridView->renderFilters()\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(343): yii\\grid\\GridView->renderTableHeader()\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(151): yii\\grid\\GridView->renderItems()\n#10 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(313): yii\\widgets\\BaseListView->renderSection(''{items}'')\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(126): yii\\grid\\GridView->renderSection(''{items}'')\n#12 [internal function]: yii\\widgets\\BaseListView->yii\\widgets\\{closure}(Array)\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\BaseListView.php(129): preg_replace_callback(''/{\\\\w+}/'', Object(Closure), ''{summary}\\n{item...'')\n#14 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\grid\\GridView.php(288): yii\\widgets\\BaseListView->run()\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Widget.php(102): yii\\grid\\GridView->run()\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php(81): yii\\base\\Widget::widget(Array)\n#17 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#18 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#19 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#20 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''index'', Array, Object(backend\\controllers\\PekerjaanController))\n#21 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(49): yii\\base\\Controller->render(''index'', Array)\n#22 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#23 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#25 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#26 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#27 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#28 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#29 {main}'),
(168, 1, 'yii\\base\\UnknownClassException', 1475849239.8706, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\UnknownClassException'' with message ''Unable to find ''backend\\models\\Pekerjaan'' in file: C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend/models/Pekerjaan.php. Namespace missing?'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php:291\nStack trace:\n#0 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\models\\search\\PekerjaanSearch.php(14): spl_autoload_call(''backend\\\\models\\\\...'')\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\BaseYii.php(288): include(''C:\\\\xampp\\\\htdocs...'')\n#3 [internal function]: yii\\BaseYii::autoload(''backend\\\\models\\\\...'')\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(39): spl_autoload_call(''backend\\\\models\\\\...'')\n#5 [internal function]: backend\\controllers\\PekerjaanController->actionIndex()\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''index'', Array)\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/index'', Array)\n#10 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
(169, 1, 'yii\\base\\ErrorException:4', 1475849437.254, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''->'' (T_OBJECT_OPERATOR)'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php:44\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(170, 1, 'yii\\base\\ErrorException:4', 1475850223.3284, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''{'''' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(171, 1, 'yii\\base\\ErrorException:4', 1475850231.1695, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''{'''' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:48\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(172, 1, 'yii\\base\\ErrorException:4', 1475850238.6837, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected '';'', expecting '']'''' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:86\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(173, 1, 'yii\\base\\ErrorException:4', 1475850257.8728, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected '';'', expecting '']'''' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:86\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(174, 1, 'yii\\base\\ErrorException:4', 1475850261.1303, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/index]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected '';'', expecting '']'''' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\index.php:86\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(175, 1, 'yii\\base\\ErrorException:4', 1475850432.3399, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''syntax error, unexpected ''::'' (T_PAAMAYIM_NEKUDOTAYIM), expecting '','' or '';'''' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:53\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(176, 1, 'yii\\base\\ErrorException:4096', 1475850475.9426, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Object of class common\\models\\User could not be converted to string'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php:537\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(537): yii\\base\\ErrorHandler->handleError(4096, ''Object of class...'', ''C:\\\\xampp\\\\htdocs...'', 537, Array)\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1238): yii\\helpers\\BaseHtml::input(''hidden'', ''Pekerjaan[creat...'', Object(common\\models\\User), Array)\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1300): yii\\helpers\\BaseHtml::activeInput(''hidden'', Object(backend\\models\\Pekerjaan), ''created_by'', Array)\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(392): yii\\helpers\\BaseHtml::activeHiddenInput(Object(backend\\models\\Pekerjaan), ''created_by'', Array)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(53): yii\\widgets\\ActiveField->hiddenInput(Array)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}'),
(177, 1, 'yii\\base\\ErrorException:4096', 1475850580.3066, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Object of class common\\models\\User could not be converted to string'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:53\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(53): yii\\base\\ErrorHandler->handleError(4096, ''Object of class...'', ''C:\\\\xampp\\\\htdocs...'', 53, Array)\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\base\\Controller->render(''create'', Array)\n#10 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#12 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#14 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#17 {main}'),
(178, 1, 'yii\\base\\UnknownMethodException', 1475850665.6067, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownMethodException'' with message ''Calling unknown method: yii\\web\\User::identity__toString()'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:285\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(53): yii\\base\\Component->__call(''identity__toStr...'', Array)\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(53): yii\\web\\User->identity__toString()\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#10 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\base\\Controller->render(''create'', Array)\n#11 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#12 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#14 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#17 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#18 {main}'),
(179, 1, 'yii\\base\\ErrorException:1', 1475850680.5138, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Call to undefined function toString()'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:53\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
(180, 1, 'yii\\base\\ErrorException:4096', 1475850691.8886, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Object of class common\\models\\User could not be converted to string'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php:537\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(537): yii\\base\\ErrorHandler->handleError(4096, ''Object of class...'', ''C:\\\\xampp\\\\htdocs...'', 537, Array)\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1238): yii\\helpers\\BaseHtml::input(''hidden'', ''Pekerjaan[creat...'', Object(common\\models\\User), Array)\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\helpers\\BaseHtml.php(1300): yii\\helpers\\BaseHtml::activeInput(''hidden'', Object(backend\\models\\Pekerjaan), ''created_by'', Array)\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(392): yii\\helpers\\BaseHtml::activeHiddenInput(Object(backend\\models\\Pekerjaan), ''created_by'', Array)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(53): yii\\widgets\\ActiveField->hiddenInput(Array)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#10 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#12 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\base\\Controller->render(''create'', Array)\n#14 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#17 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#18 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#19 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#20 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#21 {main}');
INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
(181, 1, 'yii\\base\\ErrorException:4096', 1475850745.1191, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\ErrorException'' with message ''Object of class common\\models\\User could not be converted to string'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php:55\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(55): yii\\base\\ErrorHandler->handleError(4096, ''Object of class...'', ''C:\\\\xampp\\\\htdocs...'', 55, Array)\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\base\\Controller->render(''create'', Array)\n#10 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#12 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#14 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#17 {main}'),
(182, 1, 'yii\\base\\UnknownPropertyException', 1475850799.0619, '[backend][/neo/yii2-starter-kit-master/backend/web/pekerjaan/create]', 'exception ''yii\\base\\UnknownPropertyException'' with message ''Getting unknown property: yii\\web\\User::nip'' in C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Component.php:143\nStack trace:\n#0 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\_form.php(54): yii\\base\\Component->__get(''nip'')\n#1 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#2 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#3 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, NULL)\n#4 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\views\\pekerjaan\\create.php(17): yii\\base\\View->render(''_form'', Array)\n#5 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(''C:\\\\xampp\\\\htdocs...'')\n#6 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(''C:\\\\xampp\\\\htdocs...'', Array)\n#7 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(''C:\\\\xampp\\\\htdocs...'', Array, Object(backend\\controllers\\PekerjaanController))\n#8 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(378): yii\\base\\View->render(''create'', Array, Object(backend\\controllers\\PekerjaanController))\n#9 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\controllers\\PekerjaanController.php(97): yii\\base\\Controller->render(''create'', Array)\n#10 [internal function]: backend\\controllers\\PekerjaanController->actionCreate()\n#11 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#12 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Controller.php(154): yii\\base\\InlineAction->runWithParams(Array)\n#13 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Module.php(454): yii\\base\\Controller->runAction(''create'', Array)\n#14 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\web\\Application.php(87): yii\\base\\Module->runAction(''pekerjaan/creat...'', Array)\n#15 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#16 C:\\xampp\\htdocs\\neo\\yii2-starter-kit-master\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#17 {main}');

-- --------------------------------------------------------

--
-- Table structure for table `system_rbac_migration`
--

CREATE TABLE `system_rbac_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_rbac_migration`
--

INSERT INTO `system_rbac_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1475243970),
('m150625_214101_roles', 1475243992),
('m150625_215624_init_permissions', 1475243992),
('m151223_074604_edit_own_model', 1475243992);

-- --------------------------------------------------------

--
-- Table structure for table `timeline_event`
--

CREATE TABLE `timeline_event` (
  `id` int(11) NOT NULL,
  `application` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `timeline_event`
--

INSERT INTO `timeline_event` (`id`, `application`, `category`, `event`, `data`, `created_at`) VALUES
(1, 'frontend', 'user', 'signup', '{"public_identity":"webmaster","user_id":1,"created_at":1475243966}', 1475243966),
(2, 'frontend', 'user', 'signup', '{"public_identity":"manager","user_id":2,"created_at":1475243966}', 1475243966),
(3, 'frontend', 'user', 'signup', '{"public_identity":"user","user_id":3,"created_at":1475243966}', 1475243966),
(4, 'backend', 'user', 'signup', '{"public_identity":"manager100","user_id":4,"created_at":1475849675}', 1475849675);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_client` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_client_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '2',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `logged_at` int(11) DEFAULT NULL,
  `nip` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `access_token`, `password_hash`, `oauth_client`, `oauth_client_user_id`, `email`, `status`, `created_at`, `updated_at`, `logged_at`, `nip`) VALUES
(1, 'webmaster', '3_jMvONNce5PpC2XXUdhJm3oFJ1gjdxR', '-sUCLLWkEmUUuMH-4b5Zp60jUzAsBR2ETlyuZgWI', '$2y$13$xHz3BPI.hqogi1yhSq6oHOKmmVy1uuvi/vn19brsTs9H7JGFZp3Aa', NULL, NULL, 'webmaster@example.com', 2, 1475243967, 1475243967, 1475849631, ''),
(2, 'manager', 'VvFZKi7NKH_pEaE4bGO8Na61c9M8ErKg', 'E_Jk0ShsvKLX8YX5t9m3HvxK_QWOmx8WQMIC3rPa', '$2y$13$13xgKYdr7XZ.DBynfs6hqe4DBsYK2seSSflFj67SGQJ.kDi2GoeD2', NULL, NULL, 'manager@example.com', 2, 1475243968, 1475243968, 1475849484, ''),
(3, 'user', 'TyyKemmMTb5ooS_ndFrP_WoV-3TParBs', 'yoxdOS6MM2A7_3iZUpaB_q66kQiDXqfDEh_xmbcj', '$2y$13$5caHlqYooIPo7xCu/MBKoOCpGkVVN0xOsHqq7EI2xKshNuyadZAG6', NULL, NULL, 'user@example.com', 2, 1475243968, 1475243968, 1475850381, '2'),
(4, 'manager100', 'yo3skpPXD6eRl6X51Daqy44VqrKAApqk', 'jTArRDiQBj1ttIzvj7OA6AbZVi1zfOsx86CBDZZA', '$2y$13$Im9HqauIC4iKtiL2nOlKEusiI.gbmTRS22VDHmNmTe4b0ElR1H1Va', NULL, NULL, 'asdf@asdf.casdf', 2, 1475849675, 1475849675, 1475850430, '100');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_base_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `gender` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_id`, `firstname`, `middlename`, `lastname`, `avatar_path`, `avatar_base_url`, `locale`, `gender`) VALUES
(1, 'John', NULL, 'Doe', NULL, NULL, 'en-US', NULL),
(2, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL),
(3, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL),
(4, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `token` varchar(40) NOT NULL,
  `expire_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uses`
--

CREATE TABLE `uses` (
  `id` int(4) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_karyawan` int(4) NOT NULL,
  `otoritas` enum('Admin','Atasan','Bawahan') NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `waktu_kerja`
--

CREATE TABLE `waktu_kerja` (
  `id` int(4) NOT NULL,
  `nip` varchar(255) NOT NULL,
  `jam_awal` datetime NOT NULL,
  `jam_pulang` datetime NOT NULL,
  `deskripsi` text,
  `is_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `waktu_kerja`
--

INSERT INTO `waktu_kerja` (`id`, `nip`, `jam_awal`, `jam_pulang`, `deskripsi`, `is_deleted`) VALUES
(1, '', '2016-10-04 00:00:01', '2016-10-04 00:00:01', '1', 1),
(2, '', '2016-10-04 10:15:00', '2016-10-04 10:15:00', '1', 0),
(3, '2', '2016-10-04 09:45:48', '2016-09-27 05:45:48', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `widget_carousel`
--

CREATE TABLE `widget_carousel` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `widget_carousel`
--

INSERT INTO `widget_carousel` (`id`, `key`, `status`) VALUES
(1, 'index', 1);

-- --------------------------------------------------------

--
-- Table structure for table `widget_carousel_item`
--

CREATE TABLE `widget_carousel_item` (
  `id` int(11) NOT NULL,
  `carousel_id` int(11) NOT NULL,
  `base_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `widget_carousel_item`
--

INSERT INTO `widget_carousel_item` (`id`, `carousel_id`, `base_url`, `path`, `type`, `url`, `caption`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 'localhost/yii2-starter-kit-master/frontend/web', 'img/yii2-starter-kit.gif', 'image/gif', '/', NULL, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `widget_menu`
--

CREATE TABLE `widget_menu` (
  `id` int(11) NOT NULL,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `widget_menu`
--

INSERT INTO `widget_menu` (`id`, `key`, `title`, `items`, `status`) VALUES
(1, 'frontend-index', 'Frontend index menu', '[\n    {\n        "label": "Get started with Yii2",\n        "url": "http://www.yiiframework.com",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-success\\">{label}</a>"\n    },\n    {\n        "label": "Yii2 Starter Kit on GitHub",\n        "url": "https://github.com/trntv/yii2-starter-kit",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-primary\\">{label}</a>"\n    },\n    {\n        "label": "Find a bug?",\n        "url": "https://github.com/trntv/yii2-starter-kit/issues",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-danger\\">{label}</a>"\n    }\n]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `widget_text`
--

CREATE TABLE `widget_text` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `widget_text`
--

INSERT INTO `widget_text` (`id`, `key`, `title`, `body`, `status`, `created_at`, `updated_at`) VALUES
(1, 'backend_welcome', 'Welcome to backend', '<p>Welcome to Yii2 Starter Kit Dashboard</p>', 1, 1475243969, 1475243969),
(2, 'ads-example', 'Google Ads Example Block', '<div class="lead">\n                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\n                <ins class="adsbygoogle"\n                     style="display:block"\n                     data-ad-client="ca-pub-9505937224921657"\n                     data-ad-slot="2264361777"\n                     data-ad-format="auto"></ins>\n                <script>\n                (adsbygoogle = window.adsbygoogle || []).push({});\n                </script>\n            </div>', 0, 1475243969, 1475243969);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_article_author` (`created_by`),
  ADD KEY `fk_article_updater` (`updated_by`),
  ADD KEY `fk_article_category` (`category_id`);

--
-- Indexes for table `article_attachment`
--
ALTER TABLE `article_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_article_attachment_article` (`article_id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_article_category_section` (`parent_id`);

--
-- Indexes for table `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_storage_item`
--
ALTER TABLE `file_storage_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `i18n_message`
--
ALTER TABLE `i18n_message`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Indexes for table `i18n_source_message`
--
ALTER TABLE `i18n_source_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_posisi` (`id_posisi`),
  ADD KEY `id_departemen` (`id_departemen`);

--
-- Indexes for table `key_storage_item`
--
ALTER TABLE `key_storage_item`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `idx_key_storage_item_key` (`key`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_karyawan` (`nip`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbac_auth_assignment`
--
ALTER TABLE `rbac_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `rbac_auth_item`
--
ALTER TABLE `rbac_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `rbac_auth_item_child`
--
ALTER TABLE `rbac_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `rbac_auth_rule`
--
ALTER TABLE `rbac_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `system_db_migration`
--
ALTER TABLE `system_db_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `system_log`
--
ALTER TABLE `system_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_log_level` (`level`),
  ADD KEY `idx_log_category` (`category`);

--
-- Indexes for table `system_rbac_migration`
--
ALTER TABLE `system_rbac_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `timeline_event`
--
ALTER TABLE `timeline_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_created_at` (`created_at`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uses`
--
ALTER TABLE `uses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_karyawan` (`id_karyawan`);

--
-- Indexes for table `waktu_kerja`
--
ALTER TABLE `waktu_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget_carousel`
--
ALTER TABLE `widget_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget_carousel_item`
--
ALTER TABLE `widget_carousel_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_item_carousel` (`carousel_id`);

--
-- Indexes for table `widget_menu`
--
ALTER TABLE `widget_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget_text`
--
ALTER TABLE `widget_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_widget_text_key` (`key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `article_attachment`
--
ALTER TABLE `article_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `departemen`
--
ALTER TABLE `departemen`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `file_storage_item`
--
ALTER TABLE `file_storage_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `i18n_source_message`
--
ALTER TABLE `i18n_source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `system_log`
--
ALTER TABLE `system_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `timeline_event`
--
ALTER TABLE `timeline_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uses`
--
ALTER TABLE `uses`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `waktu_kerja`
--
ALTER TABLE `waktu_kerja`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `widget_carousel`
--
ALTER TABLE `widget_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `widget_carousel_item`
--
ALTER TABLE `widget_carousel_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `widget_menu`
--
ALTER TABLE `widget_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `widget_text`
--
ALTER TABLE `widget_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_author` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_article_category` FOREIGN KEY (`category_id`) REFERENCES `article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_article_updater` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `article_attachment`
--
ALTER TABLE `article_attachment`
  ADD CONSTRAINT `fk_article_attachment_article` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_category`
--
ALTER TABLE `article_category`
  ADD CONSTRAINT `fk_article_category_section` FOREIGN KEY (`parent_id`) REFERENCES `article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `i18n_message`
--
ALTER TABLE `i18n_message`
  ADD CONSTRAINT `fk_i18n_message_source_message` FOREIGN KEY (`id`) REFERENCES `i18n_source_message` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `karyawan_ibfk_1` FOREIGN KEY (`id_posisi`) REFERENCES `posisi` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `karyawan_ibfk_2` FOREIGN KEY (`id_departemen`) REFERENCES `departemen` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD CONSTRAINT `pekerjaan_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `karyawan` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `rbac_auth_assignment`
--
ALTER TABLE `rbac_auth_assignment`
  ADD CONSTRAINT `rbac_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rbac_auth_item`
--
ALTER TABLE `rbac_auth_item`
  ADD CONSTRAINT `rbac_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `rbac_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `rbac_auth_item_child`
--
ALTER TABLE `rbac_auth_item_child`
  ADD CONSTRAINT `rbac_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rbac_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `widget_carousel_item`
--
ALTER TABLE `widget_carousel_item`
  ADD CONSTRAINT `fk_item_carousel` FOREIGN KEY (`carousel_id`) REFERENCES `widget_carousel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
